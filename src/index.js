import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import axios from "axios";
import './index.css';
import App from './App';
import coffeeReducer from "./store/reducers/coffee";
import cartReducer from "./store/reducers/cart";

axios.defaults.baseURL = 'https://blog-187e7.firebaseio.com';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  coffee: coffeeReducer,
  cart: cartReducer
});
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
