import * as actionType from "./actionTypes";
import axios from "axios/index";

export const removeCoffeeFromCart = (id) => {
  return {type: actionType.REMOVE_FROM_CART, id};
};

export const showModal = () => {
  return {type: actionType.SHOW_MODAL};
};

export const closeModal = () => {
 return {type: actionType.CLOSE_MODAL};
};

export const coffeeOrderRequest = () => {
  return {type: actionType.COFFEE_ORDER_REQUEST};
};

export const coffeeOrderSuccess = () => {
  return {type: actionType.COFFEE_ORDER_SUCCESS};
};

export const  coffeeOrderFailure = (error) => {
  return {type: actionType.COFFEE_ORDER_FAILURE, error};
};

export const onPlaceOrder = (order) => {
  return dispatch => {
    dispatch(coffeeOrderRequest());
    axios.post('/coffee-orders.json', order).then(() => {
      dispatch(coffeeOrderSuccess());
      dispatch(closeModal());
    }, error => {
      dispatch(coffeeOrderFailure(error));
    })
  }
};