import axios from 'axios';
import * as actionType from "./actionTypes";

export const orderRequest = () => {
  return {type: actionType.ORDER_REQUEST};
};

export const orderSuccess = (data) => {
  return {type: actionType.ORDER_SUCCESS, data};
};

export const  orderFailure = (error) => {
  return {type: actionType.ORDER_FAILURE, error};
};

export const getCoffeeList = () => {
  return dispatch => {
    dispatch(orderRequest());
    axios.get('/coffee.json').then(response => {
      dispatch(orderSuccess(response.data));
    }, error => {
      dispatch(orderFailure(error));
    })
  }
};

export const addCoffeeToCart = (order) => {
  return {type: actionType.ADD_TO_CART, order};
};