export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ORDER_REQUEST = "ORDER_REQUEST";
export const ORDER_SUCCESS = "ORDER_SUCCESS";
export const ORDER_FAILURE = "ORDER_FAILURE";
export const SHOW_MODAL = "SHOW_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
export const ON_PLACE_ORDER = "ON_PLACE_ORDER";
export const COFFEE_ORDER_REQUEST = "COFFEE_ORDER_REQUEST";
export const COFFEE_ORDER_SUCCESS = "COFFEE_ORDER_SUCCESS";
export const COFFEE_ORDER_FAILURE = "COFFEE_ORDER_FAILURE";