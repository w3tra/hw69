import * as actionType from '../actions/actionTypes';

const initialState = {
  coffeeList: [],
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ORDER_SUCCESS:
      return {...state, coffeeList: action.data};
    case actionType.ORDER_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;