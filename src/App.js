import React, { Component } from 'react';
import CoffeeShop from "./containers/CoffeeShop/CoffeeShop";

class App extends Component {
  render() {
    return (
      <CoffeeShop/>
    );
  }
}

export default App;
